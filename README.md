# Cooking-Master

### Project Info

| Project Name   |  Version    |
|----------------|-------------|
| Cooking Master | 2022.3.14f1 |

| Platforms     |
|---------------|
| Android       |

### Prerequisites for opening project
* Editor version is **2022.3.14f1**
* Editor platform is **Android**
* **Git LFS** is installed and working for the project

#### Scripting Symbols
| Symbol Name           |  Use                               |
|-----------------------|------------------------------------|
| CROSS_PLATFORM_INPUT  |  Input System 2.0 - Cross Platform |
| MOBILE_INPUT          |  Input System 2.0 - Mobile Input   |